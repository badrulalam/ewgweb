angular.module( 'ngBoilerplate.pieChart', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config( $stateProvider ) {
        $stateProvider.state( 'pieChart', {
            url: '/pieChart',
            views: {
                "main": {
                    controller: 'pieChartCtrl',
                    templateUrl: 'PieChart/pieChart.tpl.html'
                }
            },
            data:{ pageTitle: 'EWG' }
        });
    })

    .controller( 'pieChartCtrl', function pieChartCtrl( $scope ) {

    })

;
