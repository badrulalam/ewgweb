angular.module('ngBoilerplate.publications', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('publications', {
                url: '/publications',
                views: {
                    "main": {
                        controller: 'PublicationsCtrl',
                        templateUrl: 'publications/publications.tpl.html'
                    }
                },
                data: {pageTitle: 'publications'}
            })
            .state('publications.eyeOnElection', {
                url: '/eyeOnElection',
                views: {
                    "publication": {
                        controller: 'EyeOnElectionsCtrl',
                        templateUrl: 'publications/views/eyeOnElections.tpl.html',
                   }
                },
                data: {pageTitle: 'Eye On Elections'}
            })

        .state('publications.ecbResources', {
            url: '/ecbResources',
           views: {
               "publication": {
                    controller: 'EcbResourcesCtrl',
                    templateUrl: 'publications/views/ecbResources.tpl.html',
                }
            },
            data: {pageTitle: 'Ecb Resources'}
        })

        .state('publications.policyBriefs', {
            url: '/policyBriefs',
            views: {
                "publication": {
                    controller: 'PolicyBriefsCtrl',
                    templateUrl: 'publications/views/policyBriefs.tpl.html',
               }
            },
            data: {pageTitle: 'Policy Briefs'}
        })




            .state('publications.preliminaryStatements', {
            url: '/preliminaryStatements',
           // views: {
           //     "main": {
                    controller: 'PreliminaryStatementsCtrl',
                    templateUrl: 'publications/views/preliminaryStatements.tpl.html',
              //  }
           // },
            data: {pageTitle: 'Preliminary Statements'}
        });
    })

    .controller('PublicationsCtrl', function PublicationsCtrl($scope) {

    })

    .controller('EyeOnElectionsCtrl', function EyeOnElectionsCtrl($scope) {

    })

    .controller('EcbResourcesCtrl', function EcbResourcesCtrl($scope) {

    })

    .controller('PolicyBriefsCtrl', function PolicyBriefsCtrl($scope) {

    })

    .controller('PreliminaryStatementsCtrl', function PreliminaryStatementsCtrl($scope) {

    })
;
