angular.module( 'ngBoilerplate', [
  'templates-app',
  'templates-common',
  'ngBoilerplate.home',
  'ngBoilerplate.about',
  'ngBoilerplate.reports',
  'ngBoilerplate.pieChart',
  'ngBoilerplate.publications',
  'ngBoilerplate.mediaCoverage',
  //'ngBoilerplate.eyeOnElections',
  //'ngBoilerplate.policyBriefs',
  //'ngBoilerplate.keyAchievements',
  //'ngBoilerplate.guidingPrinciples',
  //'ngBoilerplate.preliminaryStatements',
  //'ngBoilerplate.ecbResources',
  //'ngBoilerplate.news',
  //'ngBoilerplate.visionAndMission',

  'ngBoilerplate.appfour',
  'ngBoilerplate.appsix',
  'ngBoilerplate.appseven',
  'ngBoilerplate.appeight',
    'ngBoilerplate.citygallery',
  'ngBoilerplate.dhakathgallery',
  'ngBoilerplate.mapathongallery',
   'ui.router'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/home' );
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | EWG' ;
    }
  });
})

;

