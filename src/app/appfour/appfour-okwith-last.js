angular.module( 'ngBoilerplate.appfour', [
  'ui.router',
  'placeholders',
  'ui.bootstrap',
    'ngTable',
    'leaflet-directive',
    'duScroll',
    'ui.select'

])

.config(function config( $stateProvider ) {
  $stateProvider
      .state( 'app_four', {
    url: '/app_four',
    views: {
      "main": {
        controller: 'AppFourController',
        templateUrl: 'appfour/appfour.tpl.html'
      }
    },
    data:{ pageTitle: 'App four' }
  })

      .state( 'gender_gap', {
          url: '/gender_gap',
          views: {
              "main": {
                  controller: 'AppFourGenderGapController',
                  templateUrl: 'appfour/app_four_gender_gap.tpl.html'
              }
          },
          data:{ pageTitle: 'Gender gap' }
      })

      .state( 'new_voter', {
          url: '/new_voter',
          views: {
              "main": {
                  controller: 'AppFourNewVoterController',
                  templateUrl: 'appfour/app_four_new_voter.tpl.html'
              }
          },
          data:{ pageTitle: 'Gender gap' }
      })



  ;
})



    .controller('AppFourController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.gender_gap_open = true;
        $scope.new_voter_open = false;

        //---Table data start----------

        $http.get('http://ewg.zeteq.com:1351/district').success(function (data) {

            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 100, // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

        });


      //---Table data end------------------


        //var map_path = "assets/json/district-voter-update.json";

       //var map_path = "assets/json/af-phase-map-one.geojson";

        var map_path = "assets/json/district-voter-update-marge.json";


        function countryClick(featureSelected, leafletEvent)
        {



            var layer = leafletEvent.target;
            layer.setStyle({
                weight: 2,
                color: '#666',
                fillColor: 'black'
            });
            var popupContent = getPCONT(layer, featureSelected);
            layer.bindPopup(popupContent).openPopup();

        }




        var MouseOverStyle =
        {
            fillColor: 'white',
            weight: 3,
        };



        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl =false;
        var cl_color;



        function collectMe(feature, layer)
        {

            var defstyle;
            polygons[feature.properties.id_3] = layer;
            all_features[feature.properties.id_3] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = false;

                var MouseOutStyle = {
                    weight: 1,
                    fillColor: getColor(feature.properties.m_f_diff)
                };
                layer.setStyle(MouseOutStyle);
            });

            //layer.on("mouseover", function (e) {
            //    $scope.showme = true;
            //
            //    $scope.name = feature.properties.name;
            //    $scope.gap = feature.properties.m_f_diff;
            //    $scope.feminc = feature.properties.female_inc;
            //    $scope.femper = feature.properties.female_per;
            //    $scope.maleinc = feature.properties.male_inc_n;
            //    $scope.maleper = feature.properties.male_per;
            //    layer.setStyle(MouseOverStyle);
            //
            //});

            //layer.on("click", function (e) {
            //    $scope.showme = false;
            //    var popupContent = getPCONT(feature);
            //    layer.bindPopup(popupContent).openPopup();
            //
            //});

        }



        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }

        $scope.selectMe = function (id) {

            toMap();

            if(cl){
                var MouseOutStyle = {
                    weight: 1,
                    fillColor: cl_color
                };

                cl.setStyle(MouseOutStyle);
            }

            $location.hash('map');

            //  $anchorScroll();

            var l = polygons[id];
            l.setStyle({
                fillColor: 'black'

            });
            cl = l;
            cl_color = getColor(all_features[id].properties.m_f_diff);

            var popupContent = getPCONT(all_features[id]);
            l.bindPopup(popupContent).openPopup();

        };



        function getPCONT(featureSelected)
        {

            var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.name + "</td></tr><tr><td>Existing Voters</td><td>" + featureSelected.properties.totla_vote + "</td></tr><tr><td>New Female</td><td>" + featureSelected.properties.female_inc + "</td></tr><tr><td>New Female(%)</td><td>" + featureSelected.properties.female_per + "</td></tr><tr><td>New Male</td><td>" + featureSelected.properties.male_inc_n + "</td></tr><tr><td>New Male(%)</td><td>" + featureSelected.properties.male_per + "</td></tr><tr><td>Total New Voters</td><td>" + featureSelected.properties.totla_inc_ + "</td></tr><tr></table>";
            return cont;


        }

        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    osm: {
                        name: 'osm',
                        url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },

                },
                overlays: {
                    wms: {
                        name: 'Bangladesh',
                        type: 'wms',
                        visible: true,
                        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                        layerParams: {
                            service: 'WMS',
                            version: '1.1.0',
                            request: 'GetMap',
                            srs: 'EPSG:4326',
                            style: 'polygon',
                            layers: 'distr-map-voter-update-data',
                            format: 'image/png',
                            transparent: true
                        }
                    }
                }
            }



        });



        function countryMouseover(feature, leafletEvent) {
            var layer = leafletEvent.target;
            layer.setStyle({
                fillColor: 'white'
            });
            layer.bringToFront();
            $scope.name = feature.properties.name;
            $scope.gap = feature.properties.m_f_diff;

            $scope.femper = feature.properties.female_per;

            $scope.maleper = feature.properties.male_per;


            console.log(feature);
        }


        function getColor(code) {

            var nc = Math.floor(code);
            if (nc < 5)
            {
                return "#FE8484";
            }

            if (nc >= 5 && nc < 10)
            {
                return "#FE5C5C";
            }

            if (nc >= 10 && nc < 15)
            {
                return "#FE3939";
            }


            if (nc >= 15 && nc < 20)
            {
                return "#FE2020";
            }

            if (nc >= 20 && nc < 25)
            {
                return "#DB0000";
            }

            if (nc >= 25 && nc < 30)
            {
                return "#BA0000";
            }

            if (nc >= 30 && nc < 35)
            {
                return "#6B0000";
            }

            if (nc >= 35 && nc < 40)
            {
                return "#530000";
            }

            if (nc >= 40)
            {
                return "#250000";
            }
        }

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
        }
        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.m_f_diff),
                weight: 1,
                opacity: 1,
                color: 'green',
                //  dashArray: '3',
                fillOpacity: 0.7,

            };
        }

        //
        //$http.get(map_path).success(function (data, status) {
        //    angular.extend($scope, {
        //        geojson: {
        //            data: data,
        //            style: style,
        //            mouseout: resetHighlight,
        //            resetStyleOnMouseout: true,
        //            onEachFeature: collectMe,
        //            pointToLayer: plotLayer
        //
        //        }
        //    });
        //    $scope.features = data.properties;
        //});




        //var phase_map  =  "assets/json/af-phase-map-one.geojson";


        $http.get(map_path).success(function (data1, status) {
            angular.extend($scope, {
                geojson: {
                    data: data1,

                    selectedCountry: {},
                    onEachFeature: collectMe,
                    // filter: filterLayer,
                    pointToLayer: plotLayer,
                    style: style,
                }
            });
            $scope.features1 = data1.properties;
        });



        function plotLayer(feature, latlng){
            return L.circleMarker(latlng, {
                radius: 8,
                fillColor: "#ff7800",
                color: "#000",
                weight: 1,
                opacity: 1,
                fillOpacity: 0.8
            });
        };








    })



    .controller('AppFourGenderGapController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.gender_gap_open = true;
        $scope.new_voter_open = false;
        //$http.get('http://128.199.120.151:1337/district').success(function (data) {
        $http.get('http://ewg.zeteq.com:1351/district').success(function (data) {

            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 100, // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

        });






        var map_path = "assets/json/district-voter-update.json";



        function countryClick(featureSelected, leafletEvent)
        {



            var layer = leafletEvent.target;
            layer.setStyle({
                weight: 2,
                color: '#666',
                fillColor: 'black'
            });
            var popupContent = getPCONT(layer, featureSelected);
            layer.bindPopup(popupContent).openPopup();

        }




        var MouseOverStyle =
        {
            fillColor: 'white',
            weight: 3,
        };



        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl =false;
        var cl_color;



        function collectMe(feature, layer)
        {

            var defstyle;

            polygons[feature.properties.id_3] = layer;
            all_features[feature.properties.id_3] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = false;

                var MouseOutStyle = {
                    weight: 1,
                    fillColor: getColor(feature.properties.m_f_diff)
                };

                layer.setStyle(MouseOutStyle);

            });

            layer.on("mouseover", function (e) {
                $scope.showme = true;

                $scope.name = feature.properties.name;
                $scope.gap = feature.properties.m_f_diff;
                $scope.feminc = feature.properties.female_inc;
                $scope.femper = feature.properties.female_per;
                $scope.maleinc = feature.properties.male_inc_n;
                $scope.maleper = feature.properties.male_per;


                layer.setStyle(MouseOverStyle);

            });
            layer.on("click", function (e) {
                $scope.showme = false;
                var popupContent = getPCONT(feature);
                layer.bindPopup(popupContent).openPopup();

            });

        }



        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }

        $scope.selectMe = function (id) {

            toMap();

            if(cl){
                var MouseOutStyle = {
                    weight: 1,
                    fillColor: cl_color
                };

                cl.setStyle(MouseOutStyle);
            }

            $location.hash('map');

            //  $anchorScroll();

            var l = polygons[id];
            l.setStyle({
                fillColor: 'black'

            });
            cl = l;
            cl_color = getColor(all_features[id].properties.m_f_diff);

            var popupContent = getPCONT(all_features[id]);
            l.bindPopup(popupContent).openPopup();

        };



        function getPCONT(featureSelected)
        {

            var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.name + "</td></tr><tr><td>Existing Voters</td><td>" + featureSelected.properties.totla_vote + "</td></tr><tr><td>New Female</td><td>" + featureSelected.properties.female_inc + "</td></tr><tr><td>New Female(%)</td><td>" + featureSelected.properties.female_per + "</td></tr><tr><td>New Male</td><td>" + featureSelected.properties.male_inc_n + "</td></tr><tr><td>New Male(%)</td><td>" + featureSelected.properties.male_per + "</td></tr><tr><td>Total New Voters</td><td>" + featureSelected.properties.totla_inc_ + "</td></tr><tr></table>";
            return cont;


        }

        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    img: {
                        name: 'img',

                        url: '/images/be.png',

                        type: 'xyz'
                    },
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                },
                overlays: {
                    wms: {
                        name: 'Bangladesh',
                        type: 'wms',
                        visible: true,
                        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                        layerParams: {
                            service: 'WMS',
                            version: '1.1.0',
                            request: 'GetMap',
                            srs: 'EPSG:4326',
                            style: 'polygon',
                            layers: 'distr-map-voter-update-data',
                            format: 'image/png',
                            transparent: true
                        }
                    }
                }
            }



        });



        function countryMouseover(feature, leafletEvent) {
            var layer = leafletEvent.target;
            layer.setStyle({
                fillColor: 'white'
            });
            layer.bringToFront();
            $scope.name = feature.properties.name;
            $scope.gap = feature.properties.m_f_diff;

            $scope.femper = feature.properties.female_per;

            $scope.maleper = feature.properties.male_per;


            console.log(feature);
        }


        function getColor(code) {

            var nc = Math.floor(code);
            if (nc < 5)
            {
                return "#FE8484";
            }

            if (nc >= 5 && nc < 10)
            {
                return "#FE5C5C";
            }

            if (nc >= 10 && nc < 15)
            {
                return "#FE3939";
            }


            if (nc >= 15 && nc < 20)
            {
                return "#FE2020";
            }

            if (nc >= 20 && nc < 25)
            {
                return "#DB0000";
            }

            if (nc >= 25 && nc < 30)
            {
                return "#BA0000";
            }

            if (nc >= 30 && nc < 35)
            {
                return "#6B0000";
            }

            if (nc >= 35 && nc < 40)
            {
                return "#530000";
            }

            if (nc >= 40)
            {
                return "#250000";
            }
        }

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
        }
        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.m_f_diff),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: style,
                    mouseout: resetHighlight,
                    resetStyleOnMouseout: true,
                    onEachFeature: collectMe
                }
            });
            $scope.features = data.properties;
        });







    })


    .controller('AppFourNewVoterController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.gender_gap_open = false;
        $scope.new_voter_open = true;
        $http.get('http://ewg.zeteq.com:1351/district').success(function (data) {

            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 100, // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });


        });





//var map_path ="http://128.199.120.151:8080/geoserver/ewg/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ewg%3Adistr-map-voter-update-data&maxfeatures=350&outputformat=json";
        var map_path = "assets/json/district-voter-update.json";



        function countryClick(featureSelected, leafletEvent) {



            var layer = leafletEvent.target;
            layer.setStyle({
                weight: 2,
                color: '#666',
                fillColor: 'black'
            });
            var popupContent = getPCONT(layer, featureSelected);
            layer.bindPopup(popupContent).openPopup();

        }




        var MouseOverStyle = {
            fillColor: '#000'
        };

        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl = false;
        var cl_color;

        function collectMe(feature, layer) {

            var defstyle;

            polygons[feature.properties.id_3] = layer;
            all_features[feature.properties.id_3] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = false;

                var MouseOutStyle = {
                    fillColor: getColor(feature.properties.total_inc_)
                };

                layer.setStyle(MouseOutStyle);

            });

            layer.on("mouseover", function (e) {
                $scope.showme = true;
                $scope.name = feature.properties.name;
                $scope.totinc = feature.properties.totla_inc_;
                $scope.totper = feature.properties.total_inc_;
                $scope.tot = feature.properties.totla_vote;

                layer.setStyle(MouseOverStyle);

            });
            layer.on("click", function (e) {
                $scope.showme = false;
                var popupContent = getPCONT(feature);
                layer.bindPopup(popupContent, {offset: [0, -20]}).openPopup();

            });

        }



        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }

        $scope.selectMe = function (id) {

            toMap();

            if (cl) {
                var MouseOutStyle = {
                    weight: 1,
                    fillColor: cl_color
                };

                cl.setStyle(MouseOutStyle);
            }
            $location.hash('map');

            //  $anchorScroll();

            var l = polygons[id];
            l.setStyle({
                fillColor: 'yellow'

            });

            cl = l;
            cl_color = getColor(all_features[id].properties.total_inc_);

            var popupContent = getPCONT(all_features[id]);
            l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();

        };



        function getPCONT(featureSelected) {

            var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.name + "</td></tr><tr><td>Existing Voters</td><td>" + featureSelected.properties.totla_vote + "</td></tr><tr><td>New Female</td><td>" + featureSelected.properties.female_inc + "</td></tr><tr><td>New Female(%)</td><td>" + featureSelected.properties.female_per + "</td></tr><tr><td>New Male</td><td>" + featureSelected.properties.male_inc_n + "</td></tr><tr><td>New Male(%)</td><td>" + featureSelected.properties.male_per + "</td></tr><tr><td>Total New Voters</td><td>" + featureSelected.properties.totla_inc_ + "</td></tr><tr></table>";
            return cont;


        }

        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    paper: {
                        name: 'paper',
                        url: '/images/ricepaper.png',
                        type: 'xyz'
                    },
                    wall: {
                        name: 'wall',
                        url: '/images/wall.png',
                        type: 'xyz'
                    },
                    wood: {
                        name: 'wood',
                        url: '/images/wood_pattern.png',
                        type: 'xyz'
                    },
                    img: {
                        name: 'white',
                        url: '/images/subtle_white_feathers.png',
                        type: 'xyz'
                    },
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                },
                overlays: {
                    wms: {
                        name: 'Bangladesh',
                        type: 'wms',
                        visible: true,
                        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                        layerParams: {
                            service: 'WMS',
                            version: '1.1.0',
                            request: 'GetMap',
                            srs: 'EPSG:4326',
                            style: 'polygon',
                            layers: 'distr-map-voter-update-data',
                            format: 'image/png',
                            transparent: true
                        }
                    }
                }
            }



        });

        function countryMouseover(feature, leafletEvent) {
            var layer = leafletEvent.target;
            layer.setStyle({
                weight: 2,
                color: '#666',
                fillColor: 'white'
            });
            layer.bringToFront();
            $scope.name = feature.properties.name;
            $scope.gap = feature.properties.m_f_diff;

            $scope.femper = feature.properties.female_per;

            $scope.maleper = feature.properties.male_per;


            console.log(feature);
        }


        function getColor(code) {

            var nc = parseFloat(code);
            if (nc < 5)
            {
                return "#78D261";
            }

            if (nc >= 5 && nc < 5.5)
            {
                return "#0CA300";
            }

            if (nc >= 5.5 && nc < 6)
            {
                return "#0A8300";
            }


            if (nc >= 6 && nc < 6.5)
            {
                return "#076600";
            }

            if (nc >= 6.5 && nc < 7)
            {
                return "#054700";
            }

            if (nc >= 7 && nc < 7.5)
            {
                return "#053400";
            }



        }

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
        }
        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.total_inc_),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: style,
                    mouseout: resetHighlight,
                    resetStyleOnMouseout: true
                    , selectedCountry: {},
                    onEachFeature: collectMe
                }
            });
            $scope.features = data.properties;
        });







    })





;
