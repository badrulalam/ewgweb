angular.module('ngBoilerplate.mediaCoverage', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('mediaCoverage', {
                url: '/mediaCoverage',
                views: {
                    "main": {
                        controller: 'MediaCoverageCtrl',
                        templateUrl: 'MediaCoverage/mediaCoverage.tpl.html'
                    }
                },
                data: {pageTitle: 'EWG'}
            })
        .state('mediaCoverage.news', {
            url: '/news',
            views: {
                "mediacover": {
                    controller: 'NewsCtrl',
                    templateUrl: 'MediaCoverage/news.tpl.html',
                }
            },
            data: {pageTitle: 'News'}
        });
    })

    .controller('MediaCoverageCtrl', function ReportsCtrl($scope) {

    })

    .controller('NewsCtrl', function ReportsCtrl($scope) {

    })

;
