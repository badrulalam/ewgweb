angular.module('ngBoilerplate.about', [
    'ui.router',
    'placeholders',
    'ui.bootstrap'
])

    .config(function config($stateProvider) {
        $stateProvider
            .state('about', {
                url: '/about',
                views: {
                    "main": {
                        controller: 'AboutCtrl',
                        templateUrl: 'about/about.tpl.html'
                    }
                },
                data: {pageTitle: 'About Us?'}
            })



            .state('about.visionAndMission', {
                url: '/visionAndMission',
                views: {
                    "aboutus": {
                        controller: 'VisionAndMissionCtrl',
                        templateUrl: 'about/aboutView/visionAndMission.tpl.html',
                    }
                },
                data: {pageTitle: 'Vision And Mission'}
            })
            .state('about.guidingPrinciples', {
                url: '/guidingPrinciples',
                views: {
                    "aboutus": {
                        controller: 'GuidingPrinciplesCtrl',
                        templateUrl: 'about/aboutView/guidingPrinciples.tpl.html',
                    }
                },
                data: {pageTitle: 'Guiding Principles'}
            })

            .state('about.keyAchievements', {
                url: '/keyAchievements',
                views: {
                    "aboutus": {
                        controller: 'keyAchievementsCtrl',
                        templateUrl: 'about/aboutView/keyAchievements.tpl.html',
                    }
                },
                data: {pageTitle: 'Key Achievements'}
            });


    })

    .controller('AboutCtrl', function AboutCtrl($scope) {
        // This is simple a demo for UI Boostrap.
        $scope.dropdownDemoItems = [
            "The first choice!",
            "And another choice for you.",
            "but wait! A third!"
        ];
    })

    .controller('VisionAndMissionCtrl', function VisionAndMissionCtrl($scope) {


    })

    .controller('keyAchievementsCtrl', function keyAchievementsCtrl($scope) {

    })

    .controller( 'guidingPrinciplesCtrl', function guidingPrinciplesCtrl( $scope ) {

    })


;
