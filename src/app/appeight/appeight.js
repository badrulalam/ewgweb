angular.module( 'ngBoilerplate.appeight', [
  'ui.router',
  'placeholders',
  'ui.bootstrap',
    'ngTable',
    'leaflet-directive',
    'duScroll',
    'ui.select'

])

.config(function config( $stateProvider ) {
  $stateProvider
      .state( 'app_eight', {
    url: '/app_eight',
    views: {
      "main": {
        controller: 'AppEightController',
        templateUrl: 'appeight/app_eight.tpl.html'
      }
    },
    data:{ pageTitle: 'App eight' }
  })

      .state( 'app_eight_winer', {
          url: '/app_eight_winer',
          views: {
              "main": {
                  controller: 'AppEightWinerController',
                  templateUrl: 'appeight/app_eight_winer.tpl.html'
              }
          },
          data:{ pageTitle: 'App eight winer' }
      })

      .state( 'app_eight_win_inf', {
          url: '/app_eight',
          views: {
              "main": {
                  controller: 'AppEightWinInfController',
                  templateUrl: 'appeight/app_eight_win_inf.tpl.html'
              }
          },
          data:{ pageTitle: 'App eight WinInf' }
      })





  ;
})

    .controller('AppEightController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.zoom = 7;
        var base_color= '#C78D3B';
        var cl =false;
        var polygons = [];
        var all_features = [];

        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }
        function getCentroid(arr) {
            return arr.reduce(function (x,y) {
                return [x[0] + y[0]/arr.length, x[1] + y[1]/arr.length]
            }, [0,0])
        };

        $scope.selconst = {};

        $scope.showConst = function (selconst) {

            if(cl){
                cl.setStyle(
                    {
                        fillColor:base_color
                    });

            }

            toMap();
            $scope.zoom=9;


            $location.hash('map');

            //  $anchorScroll();

            var t = all_features[selconst.no];
            var l = polygons[selconst.no];
            l.setStyle({
                fillColor: getColor(selconst.index)

            });
            cl = l;

            leafletData.getMap().then(function(map) {
                var latlngs = [];
                for (var i in t.geometry.coordinates) {
                    var coord = t.geometry.coordinates[i];
                    for (var j in coord) {
                        var points = coord[j];
                        for (var k in points)
                        {

                            latlngs.push(L.GeoJSON.coordsToLatLng(points[k]));

                        }
                    }
                }
                alert(l.getCenter());
                map.fitBounds(t.getBounds());
                var center = getCentroid(latlngs);
                alert(center);
                map.setZoom(9);
            });


            var popupContent = getPCONT(all_features[selconst.no]);
            l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();
        };





        function getPCONT(featureSelected) {

            var cont = "<table class='table'><tr><td>Constituency</td><td>" + featureSelected.properties.constituen + "</td></tr><tr><td>Index</td><td>" + featureSelected.properties.index + "</td></tr><tr></table>";
            return cont;


        }


        function collectMe(feature, layer) {



            polygons[feature.properties.cno] = layer;
            all_features[feature.properties.cno] = feature;




        }




        $scope.disabled = undefined;
        $scope.searchEnabled = undefined;

        $scope.enable = function () {
            $scope.disabled = false;
        };

        $scope.disable = function () {
            $scope.disabled = true;
        };

        $scope.enableSearch = function () {
            $scope.searchEnabled = true;
        }

        $scope.disableSearch = function () {
            $scope.searchEnabled = false;
        }

        $http.get('assets/json/constituency.json').success(function (data) {

            $scope.const_data = data;
        });

        $scope.sel_const = "1";
        $scope.gender_gap_open = false;
        $scope.new_voter_open = true;
        $http.get('assets/json/constituency.json').success(function (data) {

            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 300, // count per page
                sorting: {
                    No: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });


        });



        var map_path = "assets/json/const-xy20.json";







        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    paper: {
                        name: 'paper',
                        url: '/images/ricepaper.png',
                        type: 'xyz'
                    },
                    wall: {
                        name: 'wall',
                        url: '/images/wall.png',
                        type: 'xyz'
                    },
                    wood: {
                        name: 'wood',
                        url: '/images/wood_pattern.png',
                        type: 'xyz'
                    },
                    img: {
                        name: 'white',
                        url: '/images/subtle_white_feathers.png',
                        type: 'xyz'
                    },
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                }
            }



        });

        $scope.color1 = "#480000";
        $scope.color2 = "#9D0000";
        $scope.color3 = "#75DB1B";
        $scope.color4 = "#11772D";





        function getColor(code) {

            var nc = parseFloat(code);


            if (nc >= 0.5 && nc < 1.5)
            {
                return   $scope.color1;
            }

            if (nc >= 1.5 && nc < 2)
            {
                return    $scope.color2;
            }


            if (nc >= 2 && nc < 2.5)
            {
                return   $scope.color3;
            }



            if (nc >= 2.5 && nc < 3)
            {
                return   $scope.color4;
            }



        }


        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.index),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: {
                        fillColor: "#977254",
                        weight: 1,
                        opacity: 1,
                        color: '#ccc'


                    },
                    resetStyleOnMouseout: true,
                    onEachFeature: collectMe

                }
            });
            $scope.features = data.properties;
        });









    })
    .controller('AppEightWinerController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.zoom = 7;
        var base_color = '#E8DCC2';
        var cl = false;
        var polygons = [];
        var all_features = [];

        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }


        $scope.selectMe = function (no, cno) {

            toMap();

            if (cl) {
                cl.setStyle(
                    {
                        fillColor: base_color
                    });

            }
            $location.hash('map');

            //  $anchorScroll();


            var t = all_features[no];
            var l = polygons[no];
            l.setStyle({
                fillColor: getColor(cno)

            });
            cl = l;


            var popupContent = getPCONT(t);
            l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();

        };




        $scope.selconst = {};

        $scope.showConst = function (selconst) {

            if (cl) {
                cl.setStyle(
                    {
                        fillColor: base_color
                    });

            }

            toMap();



            $location.hash('map');

            //  $anchorScroll();

            var t = all_features[selconst.no];
            var l = polygons[selconst.no];
            l.setStyle({
                fillColor: getColor(selconst.cno)

            });
            cl = l;

            leafletData.getMap().then(function (map) {


                map.panTo(new L.LatLng(t.properties.y, t.properties.x),{ animate: true,duration:.8,easeLinearity:.55});

                map.setZoom(9);


            });
            var popupContent = getPCONT(all_features[selconst.no]);
            var popup = L.popup()
                .setLatLng(new L.LatLng(t.properties.y, t.properties.x))
                .setContent(popupContent);

            l.bindPopup(popup).openPopup();
        };





        function getPCONT(featureSelected) {

            var cont = "<table class='table'><tr><td>Constituency</td><td>" + featureSelected.properties.constituen + "</td></tr><tr><td>CNO</td><td>" + featureSelected.properties.cno + "</td></tr><tr><td>Wining Party</td><td>" + featureSelected.properties.winning_pa + "</td></tr><tr><td>Winning Ca</td><td>" + featureSelected.properties.winning_ca + "</td></tr></table>";
            return cont;


        }





        function collectMe(feature, layer) {

            var defstyle;

            polygons[feature.properties.cno] = layer;
            all_features[feature.properties.cno] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = false;

                var MouseOutStyle = {
                    fillColor: base_color
                };

                layer.setStyle(MouseOutStyle);

            });

            layer.on("mouseover", function (e) {
                $scope.showme = true;
                var MouseOverStyle = {
                    fillColor: "brown"
                };

                layer.setStyle(MouseOverStyle);

            });
            layer.on("click", function (e) {
                $scope.showme = false;
                var popupContent = getPCONT(feature);
                layer.bindPopup(popupContent, {offset: [0, -20]}).openPopup();

            });

        }



        $http.get('assets/json/2008-elect-winer.json').success(function (data) {

            $scope.const_data = data;

            //console.log($scope.const_data);
        });

        $scope.sel_const = "1";
        $scope.gender_gap_open = false;
        $scope.new_voter_open = true;
        $http.get('assets/json/2008-elect-winer.json').success(function (data) {
            console.log(data);
            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 300, // count per page
                sorting: {
                    No: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    console.log((params.page() - 1) * params.count());
                    console.log(params.page() * params.count());
                    console.log(orderedData);

                    $defer.resolve(orderedData.features.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });


        });



        //  var map_path = "/json/const-voting-index.json";

        var map_path = "assets/json/2008-elect-winer.json";





        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                    paper: {
                        name: 'paper',
                        url: '/images/ricepaper.png',
                        type: 'xyz'
                    },
                    wall: {
                        name: 'wall',
                        url: '/images/wall.png',
                        type: 'xyz'
                    },
                    wood: {
                        name: 'wood',
                        url: '/images/wood_pattern.png',
                        type: 'xyz'
                    },
                    img: {
                        name: 'white',
                        url: '/images/subtle_white_feathers.png',
                        type: 'xyz'
                    },
                    world: {
                        name: 'map',
                        url: '/images/map.png',
                        type: 'xyz'
                    }

                }
            }



        });

        $scope.color1 = "#480000";
        $scope.color2 = "#9D0000";
        $scope.color3 = "#75DB1B";
        $scope.color4 = "#11772D";





        function getColor(code) {

            var nc = parseFloat(code);


            if (nc >= 1 && nc < 50)
            {
                return   $scope.color1;
            }

            if (nc >= 51 && nc < 100)
            {
                return    $scope.color2;
            }


            if (nc >= 101 && nc < 200)
            {
                return   $scope.color3;
            }



            if (nc >= 201 && nc < 300)
            {
                return   $scope.color4;
            }



        }


        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.cno),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: {
                        fillColor: base_color,
                        weight: 1,
                        opacity: 1,
                        color: 'white',
                        //  dashArray: '3',
                        fillOpacity: 0.7

                    },
                    resetStyleOnMouseout: true,
                    onEachFeature: collectMe

                }
            });
            $scope.features = data.properties;
        });









    })

    .controller('AppEightWinInfController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.zoom = 7;
        var base_color= '#E8DCC2';
        var cl =false;
        var polygons = [];
        var all_features = [];

        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }
        function getCentroid(arr) {
            return arr.reduce(function (x,y) {
                return [x[0] + y[0]/arr.length, x[1] + y[1]/arr.length]
            }, [0,0])
        };

        $scope.selconst = {};

        $scope.showConst = function (selconst) {

            if(cl){
                cl.setStyle(
                    {
                        fillColor:base_color
                    });

            }

            toMap();
            $scope.zoom=9;


            $location.hash('map');

            //  $anchorScroll();

            var t = all_features[selconst.no];
            var l = polygons[selconst.no];
            l.setStyle({
                fillColor: getColor(selconst.winning_pa)

            });
            cl = l;

            leafletData.getMap().then(function(map) {
                var latlngs = [];
                for (var i in t.geometry.coordinates) {
                    var coord = t.geometry.coordinates[i];
                    for (var j in coord) {
                        var points = coord[j];
                        for (var k in points)
                        {

                            latlngs.push(L.GeoJSON.coordsToLatLng(points[k]));

                        }
                    }
                }
                alert(l.getCenter());
                map.fitBounds(t.getBounds());
                var center = getCentroid(latlngs);
                alert(center);
                map.setZoom(9);
            });


            var popupContent = getPCONT(all_features[selconst.no]);
            l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();
        };





        function getPCONT(featureSelected) {

            var cont = "<table class='table'><tr><td>Constituency</td><td>" + featureSelected.properties.constituen + "</td></tr><tr><td>Index</td><td>" + featureSelected.properties.index + "</td></tr><tr></table>";
            return cont;


        }


        function collectMe(feature, layer) {



            polygons[feature.properties.cno] = layer;
            all_features[feature.properties.cno] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = false;

                var MouseOutStyle = {
                    fillColor: getColor(feature.properties.winning_pa)
                };

                layer.setStyle(MouseOutStyle);

            });

            layer.on("mouseover", function (e) {
                $scope.showme = true;
                var MouseOverStyle = {
                    fillColor: "brown"
                };

                layer.setStyle(MouseOverStyle);

            });

            layer.on("click", function (e) {
                $scope.showme = false;
                var popupContent = getPCONT(feature);
                layer.bindPopup(popupContent, {offset: [0, -20]}).openPopup();

            });


        }

        $scope.selectMe = function (no, cno) {

            toMap();

            if (cl) {
                cl.setStyle(
                    {
                        fillColor: base_color
                    });

            }
            $location.hash('map');

            //  $anchorScroll();


            var t = all_features[no];
            var l = polygons[no];
            l.setStyle({
                fillColor: getColor(winning_pa)

            });
            cl = l;


            var popupContent = getPCONT(t);
            l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();

        };



        function getPCONT(featureSelected) {

            var cont = "<table class='table'><tr><td>Constituency</td><td>" + featureSelected.properties.constituen + "</td></tr><tr><td>CNO</td><td>" + featureSelected.properties.cno + "</td></tr><tr><td>Wining Party</td><td>" + featureSelected.properties.winning_pa + "</td></tr><tr><td>Winning Ca</td><td>" + featureSelected.properties.winning_ca + "</td></tr></table>";
            return cont;


        }


        $scope.disabled = undefined;
        $scope.searchEnabled = undefined;

        $scope.enable = function () {
            $scope.disabled = false;
        };

        $scope.disable = function () {
            $scope.disabled = true;
        };

        $scope.enableSearch = function () {
            $scope.searchEnabled = true;
        }

        $scope.disableSearch = function () {
            $scope.searchEnabled = false;
        }

        $http.get('assets/json/constituency.json').success(function (data) {

            $scope.const_data = data;
        });

        $scope.sel_const = "1";
        $scope.gender_gap_open = false;
        $scope.new_voter_open = true;
        $http.get('assets/json/constituency.json').success(function (data) {

            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 300, // count per page
                sorting: {
                    No: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });


        });



        //  var map_path = "/json/const-voting-index.json";

        var map_path = "assets/json/2008-elect-winer.json";





        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    paper: {
                        name: 'paper',
                        url: '/images/ricepaper.png',
                        type: 'xyz'
                    },
                    wall: {
                        name: 'wall',
                        url: '/images/wall.png',
                        type: 'xyz'
                    },
                    wood: {
                        name: 'wood',
                        url: '/images/wood_pattern.png',
                        type: 'xyz'
                    },
                    img: {
                        name: 'white',
                        url: '/images/subtle_white_feathers.png',
                        type: 'xyz'
                    },
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                }
            }



        });

        $scope.color1 = "#A70000";
        $scope.color2 = "#FE5C5C";
        $scope.color3 = "#75DB1B";
        $scope.color4 = "#0C3110";





        function getColor(code) {

            //var nc = parseFloat(code);
            var nc = code;


            if (nc =="Bangladesh Awami League")
            {
                return   $scope.color1;
            }

            if (nc =="Bangladesh Jamaat-e-Islami")
            {
                return    $scope.color2;
            }


            if (nc=="Bangladesh Jatiya Partye")
            {
                return   $scope.color3;
            }

            if (nc =="Jatiya Party")
            {
                return   $scope.color3;
            }
            if (nc =="Independent")
            {
                return   $scope.color3;
            }

            if (nc =="Jatiya Samajtantrik Dal")
            {
                return   $scope.color3;
            }

            if (nc =="Liberal Democratic Party")
            {
                return   $scope.color3;
            }
            if (nc =="Vacant")
            {
                return   $scope.color3;
            }
            if (nc =="Workers Party")
            {
                return   $scope.color3;
            }
            if (nc =="Workers Party of Bangladesh")
            {
                return   $scope.color3;
            }




            if (nc ="Bangladesh Nationalist Party")
            {
                return   $scope.color4;
            }



        }


        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.winning_pa),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: style,
                    resetStyleOnMouseout: true,
                    onEachFeature: collectMe

                }
            });
            $scope.features = data.properties;
        });









    })




;
