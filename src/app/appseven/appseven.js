angular.module( 'ngBoilerplate.appseven', [
  'ui.router',
  'placeholders',
  'ui.bootstrap',
    'ngTable',
    'leaflet-directive',
    'duScroll',
    'ui.select'

])

.config(function config( $stateProvider ) {
  $stateProvider
      .state( 'app_seven', {
    url: '/app_seven',
    views: {
      "main": {
        controller: 'AppSevenController',
        templateUrl: 'appseven/app_seven.tpl.html'
      }
    },
    data:{ pageTitle: 'App seven' }
  })

      .state( 'seven_dist_event', {
          url: '/seven_dist_event',
          views: {
              "main": {
                  controller: 'AppSevenEventController',
                  templateUrl: 'appseven/app_seven_district_event.tpl.html'
              }
          },
          data:{ pageTitle: 'App seven event' }
      })

      .state( 'seven_dist_wounded', {
          url: '/district_wounded',
          views: {
              "main": {
                  controller: 'AppSevenDistrictWoundedController',
                  templateUrl: 'appseven/app_seven_district_wounded.tpl.html'
              }
          },
          data:{ pageTitle: 'app seven wounde' }
      })

      .state( 'seven_dist_casualties', {
          url: '/seven_dist_casualties',
          views: {
              "main": {
                  controller: 'AppSevenDistrictCasualtiesController',
                  templateUrl: 'appseven/app_seven_district_casualties.tpl.html'
              }
          },
          data:{ pageTitle: 'app seven Casualties' }
      })



  ;
})


    .controller('AppSevenController', function ($scope, $document, $location, $anchorScroll, $filter, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {




        $scope.gender_gap_open=false;
        $scope.new_voter_open=false;



        $http.get('http://ewg.zeteq.com:1351/district').success(function (data) {
            console.log('my data',data);
            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 100, // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });


        });






//var map_path ="http://128.199.120.151:8080/geoserver/ewg/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ewg%3Adistr-map-voter-update-data&maxfeatures=350&outputformat=json";
        var map_path = "assets/json/district-voter-update.json";



        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    img: {
                        name: 'img',

                        url: '/images/subtle_white_feathers.png',

                        type: 'xyz'
                    },

                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                    xyz: {
                        name: 'Water Color',
                        //      url: 'http://www.openstreetmap.org/#map=4/44.68/22.19&layers=T',
                        //    url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        //       url: 'http://subtlepatterns.com/patterns/concrete_seamless.png',
                        //     url:'   http://subtlepatterns.com/patterns/halftone.png',
                        url: 'http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },

                    nb: {
                        name: 'No Boundary',
                        url: 'http://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png  ',
                        type: 'xyz'
                    },
                    actbase: {
                        name: 'Base map',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/acetate-base/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                    rlf: {
                        name: 'Relief',
                        url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer/tile/{z}/{y}/{x}',
                        type: 'xyz'
                    },
                    dark: {
                        name: 'Dark',
                        url: 'http://{s}.basemaps.cartocdn.com/dark_nolabels/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                    Hyda: {
                        name: 'Regular',
                        url: 'http://{s}.tile.openstreetmap.se/hydda/base/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },

                    World: {
                        name: 'Imagery',
                        url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                        type: 'xyz'
                    },
                    Physical: {
                        name: 'Physical',
                        url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer/tile/{z}/{y}/{x}',
                        type: 'xyz'
                    },
                    trn: {
                        name: 'Terrain',
                        url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Terrain_Base/MapServer/tile/{z}/{y}/{x}',
                        type: 'xyz'
                    }
                },
                overlays: {
                    wms: {
                        name: 'Bangladesh',
                        type: 'wms',
                        visible: true,
                        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                        layerParams: {
                            service: 'WMS',
                            version: '1.1.0',
                            request: 'GetMap',
                            srs: 'EPSG:4326',
                            style:'polygon',
                            layers: 'distr-map-voter-update-data',
                            format: 'image/png',
                            transparent: true
                        }
                    }
                }
            }



        });


    })

    .controller('AppSevenDistrictCasualtiesController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.gender_gap_open = false;
        $scope.new_voter_open = true;
        $http.get('http://ewg.zeteq.com:1351/district').success(function (data) {

            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 100, // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });


        });





//var map_path ="http://128.199.120.151:8080/geoserver/ewg/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ewg%3Adistr-map-voter-update-data&maxfeatures=350&outputformat=json";
        var map_path = "assets/json/dist-casulties.json";



        function countryClick(featureSelected, leafletEvent) {



            var layer = leafletEvent.target;
            layer.setStyle({
                weight: 2,
                color: '#666',
                fillColor: 'black'
            });
            var popupContent = getPCONT(layer, featureSelected);
            layer.bindPopup(popupContent).openPopup();

        }




        var MouseOverStyle = {
            fillColor: '#000'
        };

        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl = false;
        var cl_color;

        function collectMe(feature, layer) {

            var defstyle;

            polygons[feature.properties.id_3] = layer;
            all_features[feature.properties.id_3] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = false;

                var MouseOutStyle = {
                    fillColor: getColor(feature.properties.casualt_nu)
                };

                layer.setStyle(MouseOutStyle);

            });

            layer.on("mouseover", function (e) {
                $scope.showme = true;
                $scope.name = feature.properties.NAME_3;
                $scope.casualtnu = feature.properties.casualt_nu;
                $scope.casualtpr = feature.properties.casualt_pr;
                //$scope.tot = feature.properties.totla_vote;

                layer.setStyle(MouseOverStyle);

            });
            layer.on("click", function (e) {
                $scope.showme = false;
                var popupContent = getPCONT(feature);
                layer.bindPopup(popupContent, {offset: [0, -20]}).openPopup();

            });

        }



        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }

        $scope.selectMe = function (id) {

            toMap();

            if (cl) {
                var MouseOutStyle = {
                    weight: 1,
                    fillColor: cl_color
                };

                cl.setStyle(MouseOutStyle);
            }
            $location.hash('map');

            //  $anchorScroll();

            var l = polygons[id];
            l.setStyle({
                fillColor: 'yellow'

            });

            cl = l;
            cl_color = getColor(all_features[id].properties.casualt_nu);

            var popupContent = getPCONT(all_features[id]);
            l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();

        };



        function getPCONT(featureSelected) {

            var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.NAME_3 + "</td></tr><tr><td>Number</td><td>" + featureSelected.properties.casualt_nu + "</td></tr><tr><td> % of Casualties</td><td>" + featureSelected.properties.casualt_pr + "</td></tr></table>";
            return cont;


        }

        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    paper: {
                        name: 'paper',
                        url: '/images/ricepaper.png',
                        type: 'xyz'
                    },
                    wall: {
                        name: 'wall',
                        url: '/images/wall.png',
                        type: 'xyz'
                    },
                    wood: {
                        name: 'wood',
                        url: '/images/wood_pattern.png',
                        type: 'xyz'
                    },
                    img: {
                        name: 'white',
                        url: '/images/subtle_white_feathers.png',
                        type: 'xyz'
                    },
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                },
                overlays: {
                    wms: {
                        name: 'Bangladesh',
                        type: 'wms',
                        visible: true,
                        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                        layerParams: {
                            service: 'WMS',
                            version: '1.1.0',
                            request: 'GetMap',
                            srs: 'EPSG:4326',
                            style: 'polygon',
                            layers: 'distr-map-voter-update-data',
                            format: 'image/png',
                            transparent: true
                        }
                    }
                }
            }



        });

        //function countryMouseover(feature, leafletEvent) {
        //    var layer = leafletEvent.target;
        //    layer.setStyle({
        //        weight: 2,
        //        color: '#666',
        //        fillColor: 'white'
        //    });
        //    layer.bringToFront();
        //    $scope.name = feature.properties.name;
        //    $scope.gap = feature.properties.m_f_diff;
        //
        //    $scope.femper = feature.properties.female_per;
        //
        //    $scope.maleper = feature.properties.male_per;
        //
        //
        //    console.log(feature);
        //}


        function getColor(code) {

            var nc = parseFloat(code);
            if (nc < 1)
            {
                return "#04756F";
            }

            if (nc >= 1 && nc < 100)
            {
                return "#FF8C00";
            }

            if (nc >= 101 && nc < 200)
            {
                return "#FF2D00";
            }


            if (nc >= 201)
            {
                return "#D90000";
            }



        }

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
        }
        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.casualt_nu),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: style,
                    mouseout: resetHighlight,
                    resetStyleOnMouseout: true
                    , selectedCountry: {},
                    onEachFeature: collectMe
                }
            });
            $scope.features = data.properties;
        });







    })

    .controller('AppSevenDistrictWoundedController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.gender_gap_open = true;
        $scope.new_voter_open = false;
        $http.get('http://ewg.zeteq.com:1351/district').success(function (data) {

            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 100, // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

        });





//var map_path ="http://128.199.120.151:8080/geoserver/ewg/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ewg%3Adistr-map-voter-update-data&maxfeatures=350&outputformat=json";
        var map_path = "assets/json/dist-wounded.json";



        function countryClick(featureSelected, leafletEvent)
        {



            var layer = leafletEvent.target;
            layer.setStyle({
                weight: 2,
                color: '#666',
                fillColor: 'black'
            });
            var popupContent = getPCONT(layer, featureSelected);
            layer.bindPopup(popupContent).openPopup();

        }




        var MouseOverStyle =
        {
            fillColor: 'white',
            weight: 3,
        };



        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl =false;
        var cl_color;



        function collectMe(feature, layer)
        {

            var defstyle;

            polygons[feature.properties.id_3] = layer;
            all_features[feature.properties.id_3] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = false;

                var MouseOutStyle = {
                    weight: 1,
                    fillColor: getColor(feature.properties.wounded_nu)
                };

                layer.setStyle(MouseOutStyle);

            });

            layer.on("mouseover", function (e) {
                $scope.showme = true;

                $scope.name = feature.properties.NAME_3;
                $scope.woundednu = feature.properties.wounded_nu;
                $scope.woundedpr = feature.properties.wounded_pr;
                //$scope.femper = feature.properties.female_per;
                // $scope.maleinc = feature.properties.male_inc_n;
                // $scope.maleper = feature.properties.male_per;


                layer.setStyle(MouseOverStyle);

            });
            layer.on("click", function (e) {
                $scope.showme = false;
                var popupContent = getPCONT(feature);
                layer.bindPopup(popupContent).openPopup();

            });

        }



        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }

        $scope.selectMe = function (id) {

            toMap();

            if(cl){
                var MouseOutStyle = {
                    weight: 1,
                    fillColor: cl_color
                };

                cl.setStyle(MouseOutStyle);
            }

            $location.hash('map');

            //  $anchorScroll();

            var l = polygons[id];
            l.setStyle({
                fillColor: 'black'

            });
            cl = l;
            cl_color = getColor(all_features[id].properties.wounded_nu);

            var popupContent = getPCONT(all_features[id]);
            l.bindPopup(popupContent).openPopup();

        };



        function getPCONT(featureSelected)
        {

            var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.NAME_3 + "</td></tr><tr><td>Number</td><td>" + featureSelected.properties.wounded_nu + "</td></tr><tr><td>% of wounded</td><td>" + featureSelected.properties.wounded_pr + "</td></tr></table>";
            return cont;


        }

        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    img: {
                        name: 'img',

                        url: '/images/be.png',

                        type: 'xyz'
                    },
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                },
                overlays: {
                    wms: {
                        name: 'Bangladesh',
                        type: 'wms',
                        visible: true,
                        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                        layerParams: {
                            service: 'WMS',
                            version: '1.1.0',
                            request: 'GetMap',
                            srs: 'EPSG:4326',
                            style: 'polygon',
                            layers: 'distr-map-voter-update-data',
                            format: 'image/png',
                            transparent: true
                        }
                    }
                }
            }



        });



        //function countryMouseover(feature, leafletEvent) {
        //    var layer = leafletEvent.target;
        //    layer.setStyle({
        //        fillColor: 'white'
        //    });
        //    layer.bringToFront();
        //    $scope.name = feature.properties.name;
        //    $scope.gap = feature.properties.m_f_diff;
        //
        //    $scope.femper = feature.properties.female_per;
        //
        //    $scope.maleper = feature.properties.male_per;
        //
        //
        //    console.log(feature);
        //}


        function getColor(code) {

            var nc = Math.floor(code);

            if (nc < 1)
            {
                return "#04756F";
            }


            if (nc >= 1000 && nc <= 3000)
            {
                return "#FF8C00";
            }

            if (nc >= 3001 && nc < 5000)
            {
                return "#FF2D00";
            }

            if (nc >= 5001)
            {
                return "#D90000";
            }
        }

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
        }
        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.wounded_nu),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: style,
                    mouseout: resetHighlight,
                    resetStyleOnMouseout: true,
                    onEachFeature: collectMe
                }
            });
            $scope.features = data.properties;
        });







    })

    .controller('AppSevenEventController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

        $scope.gender_gap_open = false;
        $scope.new_voter_open = true;
        $http.get('http://ewg.zeteq.com:1351/district').success(function (data) {

            $scope.tableParams = new ngTableParams({
                page: 1, // show first page
                count: 100, // count per page
                sorting: {
                    name: 'asc'     // initial sorting
                }
            }, {
                total: data.length, // length of data
                getData: function ($defer, params) {
                    // use build-in angular filter
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(data, params.orderBy()) :
                        data;

                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });


        });





//var map_path ="http://128.199.120.151:8080/geoserver/ewg/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=ewg%3Adistr-map-voter-update-data&maxfeatures=350&outputformat=json";
        var map_path = "assets/json/districtevent.json";



        function countryClick(featureSelected, leafletEvent) {



            var layer = leafletEvent.target;
            layer.setStyle({
                weight: 2,
                color: '#666',
                fillColor: 'black'
            });
            var popupContent = getPCONT(layer, featureSelected);
            layer.bindPopup(popupContent).openPopup();

        }




        var MouseOverStyle = {
            fillColor: '#000'
        };

        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl = false;
        var cl_color;

        function collectMe(feature, layer) {

            var defstyle;

            polygons[feature.properties.id_3] = layer;
            all_features[feature.properties.id_3] = feature;

            layer.on("mouseout", function (e) {
                $scope.showme = false;

                var MouseOutStyle = {
                    fillColor: getColor(feature.properties.event_numb)
                };

                layer.setStyle(MouseOutStyle);

            });

            layer.on("mouseover", function (e) {
                $scope.showme = true;
                $scope.name = feature.properties.NAME_3;
                $scope.evnumber = feature.properties.event_numb;
                $scope.evperc = feature.properties.event_perc;
                //$scope.tot = feature.properties.totla_vote;

                layer.setStyle(MouseOverStyle);

            });
            layer.on("click", function (e) {
                $scope.showme = false;
                var popupContent = getPCONT(feature);
                layer.bindPopup(popupContent, {offset: [0, -20]}).openPopup();

            });

        }



        function toMap() {
            var duration = 700; //milliseconds
            var offset = 30;
            var map = angular.element(document.getElementById('map'));
            $document.scrollToElementAnimated(map, offset, duration);


        }

        $scope.selectMe = function (id) {

            toMap();

            if (cl) {
                var MouseOutStyle = {
                    weight: 1,
                    fillColor: cl_color
                };

                cl.setStyle(MouseOutStyle);
            }
            $location.hash('map');

            //  $anchorScroll();

            var l = polygons[id];
            l.setStyle({
                fillColor: 'yellow'

            });

            cl = l;
            cl_color = getColor(all_features[id].properties.event_numb);

            var popupContent = getPCONT(all_features[id]);
            l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();

        };



        function getPCONT(featureSelected) {

            var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.NAME_3 + "</td></tr><tr><td>Event</td><td>" + featureSelected.properties.event_numb + "</td></tr><tr><td>% of Event</td><td>" + featureSelected.properties.event_perc + "</td></tr></table>";
            return cont;


        }

        angular.extend($scope, {
            bd: {
                lat: 23.7000,
                lng: 90.3500,
                zoom: 7
            },
            defaults: {
                scrollWheelZoom: true,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    paper: {
                        name: 'paper',
                        url: '/images/ricepaper.png',
                        type: 'xyz'
                    },
                    wall: {
                        name: 'wall',
                        url: '/images/wall.png',
                        type: 'xyz'
                    },
                    wood: {
                        name: 'wood',
                        url: '/images/wood_pattern.png',
                        type: 'xyz'
                    },
                    img: {
                        name: 'white',
                        url: '/images/subtle_white_feathers.png',
                        type: 'xyz'
                    },
                    act: {
                        name: 'Acetate',
                        url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                },
                overlays: {
                    wms: {
                        name: 'Bangladesh',
                        type: 'wms',
                        visible: true,
                        url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                        layerParams: {
                            service: 'WMS',
                            version: '1.1.0',
                            request: 'GetMap',
                            srs: 'EPSG:4326',
                            style: 'polygon',
                            layers: 'distr-map-voter-update-data',
                            format: 'image/png',
                            transparent: true
                        }
                    }
                }
            }



        });

        //function countryMouseover(feature, leafletEvent) {
        //    var layer = leafletEvent.target;
        //    layer.setStyle({
        //        weight: 2,
        //        color: '#666',
        //        fillColor: 'white'
        //    });
        //    layer.bringToFront();
        //    $scope.name = feature.properties.NAME_3;
        //    $scope.gap = feature.properties.m_f_diff;
        //
        //    $scope.femper = feature.properties.female_per;
        //
        //    $scope.maleper = feature.properties.male_per;
        //
        //
        //    console.log(feature);
        //}


        function getColor(code) {

            var nc = parseFloat(code);
            if (nc < 1)
            {
                return "#CDE855";
            }

            if (nc >= 1 && nc < 501)
            {
                return "#FF8C00";
            }

            if (nc >= 501 && nc < 1000)
            {
                return "#FE5C5C";
            }


            if (nc >= 1001 && nc < 3000)
            {
                return "#D90000";
            }





        }

        function resetHighlight(e) {
            geojson.resetStyle(e.target);
        }
        function style(feature)
        {
            return {
                fillColor: getColor(feature.properties.event_numb),
                weight: 1,
                opacity: 1,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }


        $http.get(map_path).success(function (data, status) {
            angular.extend($scope, {
                geojson: {
                    data: data,
                    style: style,
                    mouseout: resetHighlight,
                    resetStyleOnMouseout: true
                    , selectedCountry: {},
                    onEachFeature: collectMe
                }
            });
            $scope.features = data.properties;
        });







    })



;
