angular.module( 'ngBoilerplate.mapathongallery', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'mapathongallery', {
    url: '/mapathongallery',
    views: {
      "main": {
        controller: 'MapathongalleryCtrl',
        templateUrl: 'mapathongallery/mapathongallery.tpl.html'
      }
    },
    data:{ pageTitle: 'Mapathon Gallery ' }
  });
})

.controller( 'MapathongalleryCtrl', function AboutCtrl( $scope ) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    "The first choice!",
    "And another choice for you.",
    "but wait! A third!"
  ];
})

    .controller('ModalDemoCtrl', function ModalDemoCtrl ($scope, $modal, $log) {

      //$scope.items = [];

      $scope.animationsEnabled = true;

      $scope.open = function (size) {
        console.log(size);
        $scope.items = size;

        var modalInstance = $modal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'myModalContent.html',
          controller: 'ModalInstanceCtrl',
          size: size,
          resolve: {
            items: function () {
              return $scope.items;
            }
          }
        });

        modalInstance.result.then(function (selectedItem) {
          $scope.selected = selectedItem;
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
      };

    })

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

.controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

  $scope.items = items;
  //$scope.selected = {
  //  item: $scope.items[0]
  //};

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});